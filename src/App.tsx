import React from 'react';
import './App.css';

export function toBinary(
  input: string | number | bigint | boolean,
  padding = 4
) {
  return BigInt(input).toString(2).padStart(padding, '0');
}

const SUPER_ADMIN = BigInt(0x3ffffffffffff);
const TENNAT_ADMIN = BigInt(0x3ffffffff);
const SITE_MANAGER = BigInt(0x3ffffff);
const SITE_EMPLOYEE = BigInt(0x3fff123);

const Permision: React.FC<{ name: string, value: string, onChange: (value: number) => void }> = ({ name, value, onChange }) => {
  return (
    <div>
      <label htmlFor={name}>{name}</label>
      <input 
        id={name}
        type='checkbox'
        checked={Boolean(Number(value))}
        onChange={e => onChange(Number(e.target.checked))
      } />
    </div>
  );
};

const Permisions: React.FC = () => {
  const [permissions, setPermissions] = React.useState(BigInt(0));
  const [permissionsMax, setPermissionsMax] = React.useState(50);
  const permissionsArr = React.useMemo(
    () => toBinary(permissions, permissionsMax).split(''), 
    [permissions, permissionsMax]
  );

  const handleChange = (value: number, index: number) => {
    const nextVal = BigInt(value ? 1 : -1) * BigInt(2**index);
    setPermissions(perm => perm + nextVal)
  };

  return (
    <>
      <h1>Permissions Demo</h1>
      <h4>This app demonstrates how to transport, read and write user permissions</h4>
      <p>Below is a input field for number of user permissions and afterwards are preconfigured user types.</p>
      
      <div>
        <label htmlFor='maxPermissions'>Number of permisssions</label>
        <input 
          id="maxPermissions" 
          type="number"
          min={0}
          max={100}
          value={permissionsMax}
          onChange={e => setPermissionsMax(Number(e.target.value))}>
        </input>
      </div>

      <p style={{ textAlign: 'left' }}>
        hex:{permissions.toString(16)}  
        <br/>dec:{permissions.toString(10)} 
        <br/>bin:{permissionsArr.join('').toString()}
      </p>

      <div className='users'>
        <p>Super admin <button onClick={() => setPermissions(SUPER_ADMIN)}>SET</button></p>
        <p>Tennat admin <button onClick={() => setPermissions(TENNAT_ADMIN)}>SET</button></p>
        <p>Site manager <button onClick={() => setPermissions(SITE_MANAGER)}>SET</button></p>
        <p>Site employee <button onClick={() => setPermissions(SITE_EMPLOYEE)}>SET</button></p>
      </div>

      <div className='permissions'>
        {permissionsArr
          .map((value, _index) => {
            const index = (permissionsArr.length - 1) - _index;
            return <Permision value={value} name={index+ 'X'} key={index} onChange={value => handleChange(value, index)} />
          })}
      </div>
    </>
  );
}

function App() {
  return (
    <Permisions />
  );
}

export default App;
