import { toBinary } from './App';

describe('bigint', () => {
  it('converts to bin from string', () => {
    expect(toBinary('0')).toBe('0000');
    expect(toBinary('7')).toBe('0111');
    expect(toBinary('7', 8)).toBe('00000111');
  });

  it('converts to bin from number', () => {
    expect(toBinary(0)).toBe('0000');
    expect(toBinary(7)).toBe('0111');
    expect(toBinary(7, 8)).toBe('00000111');
  });

    it('converts to bin from bigInt', () => {
    expect(toBinary(BigInt(7))).toBe('0111');
    expect(toBinary(BigInt(7), 8)).toBe('00000111');
  });

    it('converts to bin from number', () => {
    expect(toBinary(false)).toBe('0000');
    expect(toBinary(true)).toBe('0001');
    expect(toBinary(true, 8)).toBe('00000001');
  });
});
